﻿--
-- Script was generated by Devart dbForge Studio for MySQL, Version 8.0.40.0
-- Product home page: http://www.devart.com/dbforge/mysql/studio
-- Script date 05/09/2022 1:50:53
-- Server version: 5.5.5-10.1.40-MariaDB
-- Client version: 4.1
--

-- 
-- Disable foreign keys
-- 
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;

-- 
-- Set SQL mode
-- 
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- 
-- Set character set the client will use to send SQL statements to the server
--
SET NAMES 'utf8';

DROP DATABASE IF EXISTS managebarbershopv3;

CREATE OR REPLACE DATABASE managebarbershopv3
	CHARACTER SET latin1
	COLLATE latin1_swedish_ci;

--
-- Set default database
--
USE managebarbershopv3;

--
-- Create table `proveedores`
--
CREATE OR REPLACE TABLE proveedores (
  IDproveedor int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(20) NOT NULL,
  localizacion varchar(50) NOT NULL,
  telefono varchar(9) NOT NULL,
  PRIMARY KEY (IDproveedor)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 3276,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `productos`
--
CREATE OR REPLACE TABLE productos (
  IDproducto int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(250) NOT NULL,
  costeProveedores float(4, 2) NOT NULL,
  costeCliente float(4, 2) NOT NULL,
  cantidad int(11) NOT NULL,
  PRIMARY KEY (IDproducto)
)
ENGINE = INNODB,
AUTO_INCREMENT = 6,
AVG_ROW_LENGTH = 3276,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `productos_compras`
--
CREATE OR REPLACE TABLE productos_compras (
  IDproductoCompra int(11) NOT NULL AUTO_INCREMENT,
  fecha date NOT NULL,
  IDproducto_compra_proveedor int(11) DEFAULT NULL,
  IDproducto_compra_producto int(11) DEFAULT NULL,
  PRIMARY KEY (IDproductoCompra)
)
ENGINE = INNODB,
AUTO_INCREMENT = 99,
AVG_ROW_LENGTH = 174,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE productos_compras
ADD CONSTRAINT fk_productos_compras_productos FOREIGN KEY (IDproducto_compra_producto)
REFERENCES productos (IDproducto);

--
-- Create foreign key
--
ALTER TABLE productos_compras
ADD CONSTRAINT fk_productos_compras_proveedores FOREIGN KEY (IDproducto_compra_proveedor)
REFERENCES proveedores (IDproveedor);

--
-- Create table `servicios`
--
CREATE OR REPLACE TABLE servicios (
  IDservicio int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  coste float(4, 2) NOT NULL,
  PRIMARY KEY (IDservicio)
)
ENGINE = INNODB,
AUTO_INCREMENT = 7,
AVG_ROW_LENGTH = 2730,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `clientes`
--
CREATE OR REPLACE TABLE clientes (
  IDcliente int(11) NOT NULL AUTO_INCREMENT,
  nombre varchar(50) NOT NULL,
  PRIMARY KEY (IDcliente)
)
ENGINE = INNODB,
AUTO_INCREMENT = 50,
AVG_ROW_LENGTH = 442,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create table `telefonos`
--
CREATE OR REPLACE TABLE telefonos (
  IDtelefono int(11) NOT NULL AUTO_INCREMENT,
  telefono1 varchar(9) NOT NULL,
  telefono2 varchar(9) NOT NULL,
  IDtelefono_cliente int(11) DEFAULT NULL,
  PRIMARY KEY (IDtelefono)
)
ENGINE = INNODB,
AUTO_INCREMENT = 25,
AVG_ROW_LENGTH = 1024,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE telefonos
ADD CONSTRAINT fk_telefonos_clientes FOREIGN KEY (IDtelefono_cliente)
REFERENCES clientes (IDcliente);

--
-- Create table `productos_ventas`
--
CREATE OR REPLACE TABLE productos_ventas (
  IDproductoVenta int(11) NOT NULL AUTO_INCREMENT,
  fecha date NOT NULL,
  IDproducto_venta_cliente int(11) DEFAULT NULL,
  IDproducto_venta_producto int(11) DEFAULT NULL,
  PRIMARY KEY (IDproductoVenta)
)
ENGINE = INNODB,
AUTO_INCREMENT = 104,
AVG_ROW_LENGTH = 162,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE productos_ventas
ADD CONSTRAINT fk_productos_ventas_clientes FOREIGN KEY (IDproducto_venta_cliente)
REFERENCES clientes (IDcliente);

--
-- Create foreign key
--
ALTER TABLE productos_ventas
ADD CONSTRAINT fk_productos_ventas_productos FOREIGN KEY (IDproducto_venta_producto)
REFERENCES productos (IDproducto);

--
-- Create table `citas`
--
CREATE OR REPLACE TABLE citas (
  IDcitas int(11) NOT NULL AUTO_INCREMENT,
  fecha date NOT NULL,
  hora time NOT NULL,
  IDcitas_cliente int(11) DEFAULT NULL,
  IDcitas_servicio int(11) DEFAULT NULL,
  PRIMARY KEY (IDcitas)
)
ENGINE = INNODB,
AUTO_INCREMENT = 96,
AVG_ROW_LENGTH = 180,
CHARACTER SET latin1,
COLLATE latin1_swedish_ci;

--
-- Create foreign key
--
ALTER TABLE citas
ADD CONSTRAINT fk_citas_clientes FOREIGN KEY (IDcitas_cliente)
REFERENCES clientes (IDcliente);

--
-- Create foreign key
--
ALTER TABLE citas
ADD CONSTRAINT fk_servicios_citas FOREIGN KEY (IDcitas_servicio)
REFERENCES servicios (IDservicio);

-- 
-- Dumping data for table proveedores
--
INSERT INTO proveedores VALUES
(1, 'Raul', 'Torrelavega', '622222222'),
(2, 'David', 'Santander', '611222331'),
(3, 'Jorge', 'Peñacastillo', '622333222'),
(4, 'Jose luis', 'Madrid', '625252525'),
(5, 'Pepe', 'Santoña', '655555555');

-- 
-- Dumping data for table productos
--
INSERT INTO productos VALUES
(1, 'Cera de pelo', 7.50, 15.96, 57),
(2, 'Cera de pelo Extra', 9.00, 18.95, 93),
(3, 'Tinte para el pelo', 25.00, 45.95, 60),
(4, 'Aceite para el pelo', 10.15, 22.50, 38),
(5, 'Champoo', 7.45, 12.45, -2);

-- 
-- Dumping data for table servicios
--
INSERT INTO servicios VALUES
(1, 'Corte Pelo', 14.00),
(2, 'Barba', 8.00),
(3, 'Tinte de pelo', 50.00),
(4, 'Corte pelo niño', 10.00),
(5, 'Cejas', 6.00),
(6, 'tinte de barba', 18.00);

-- 
-- Dumping data for table clientes
--
INSERT INTO clientes VALUES
(1, 'Santos junior'),
(2, 'Antony'),
(3, 'Cesar'),
(4, 'Gonzalo'),
(5, 'Macarena'),
(6, 'Pedro luiz'),
(7, 'Aselmo'),
(8, 'mari pili'),
(9, 'juan david'),
(10, 'cesarkj'),
(19, 'MARIO GONZALEZ'),
(20, 'carlejas'),
(21, 'prueba'),
(26, 'Ezequiel'),
(27, 'Ezequiel'),
(28, 'Ezequiel'),
(29, 'ezeuqiel'),
(30, 'Ezequiel'),
(31, 'sfsd'),
(32, 'Ezequiel'),
(33, 'asd'),
(34, 'ezeuqiel'),
(35, 'maria'),
(36, 'isabel'),
(37, 'charly'),
(38, 'pedro'),
(39, 'cesarario'),
(40, 'pruebaas'),
(41, 'juan manuel'),
(42, 'Marius'),
(43, 'lola'),
(44, 'pepe'),
(45, 'marina'),
(46, 'marinaa'),
(47, 'mario soberon'),
(48, 'Maria Isabel'),
(49, 'ramiro');

-- 
-- Dumping data for table telefonos
--
INSERT INTO telefonos VALUES
(1, '634811811', '734811811', 19),
(2, '634811833', '734811811', 2),
(3, '634811855', '734811811', 3),
(4, '634811867', '734811812', 4),
(6, '644701370', '625505050', 5),
(7, '699887788', '699887787', 8),
(8, '695885588', '644115522', 9),
(16, '686508938', '686508938', 19),
(17, '686508938', '686508938', 20),
(18, '666666666', '666666666', 33),
(19, '611111111', '', 40),
(20, '626626222', '', 1),
(21, '654478852', '', 41),
(22, '666666666', '', 42),
(23, '656666666', '', 46),
(24, '622222222', '', 47);

-- 
-- Dumping data for table productos_ventas
--
INSERT INTO productos_ventas VALUES
(1, '2022-08-26', 1, 2),
(2, '2022-08-17', 2, 3),
(3, '2022-08-17', 3, 5),
(4, '2022-08-18', 4, 4),
(5, '2022-08-18', 1, 1),
(6, '2022-08-26', 1, 1),
(7, '2022-08-26', 5, 3),
(8, '2022-08-31', 2, 1),
(9, '2022-08-31', 2, 1),
(10, '2022-08-31', 3, 1),
(11, '2022-08-31', 2, 1),
(13, '2022-08-31', 1, 1),
(14, '2022-08-31', 3, 1),
(15, '2022-08-31', 1, 1),
(16, '2022-08-31', 1, 1),
(17, '2022-08-31', 1, 1),
(18, '2022-08-31', 1, 1),
(19, '2022-09-03', 1, 1),
(20, '2022-09-04', 3, 1),
(21, '2022-09-04', 1, 1),
(22, '2022-09-04', 4, 5),
(23, '2022-03-03', 3, 5),
(24, '2022-01-19', 4, 4),
(25, '2022-05-18', 2, 5),
(26, '2022-06-16', 2, 5),
(27, '2022-02-07', 1, 5),
(28, '2022-07-19', 1, 5),
(29, '2022-07-06', 2, 5),
(30, '2022-10-03', 1, 5),
(31, '2022-10-19', 2, 5),
(32, '2022-10-19', 4, 5),
(33, '2022-11-07', 2, 4),
(34, '2022-11-14', 3, 5),
(35, '2022-10-17', 2, 1),
(36, '2022-11-29', 1, 1),
(37, '2022-04-06', 2, 1),
(38, '2022-04-20', 2, 5),
(39, '2022-05-10', 2, 5),
(40, '2022-05-04', 2, 5),
(41, '2022-11-17', 2, 4),
(42, '2022-12-02', 1, 5),
(43, '2022-12-30', 4, 4),
(44, '2022-12-14', 2, 5),
(45, '2022-12-15', 5, 5),
(46, '2022-12-22', 2, 5),
(47, '2022-12-06', 2, 4),
(48, '2022-12-31', 6, 5),
(49, '2022-01-03', 2, 5),
(50, '2022-01-06', 2, 5),
(51, '2022-01-26', 2, 5),
(52, '2022-01-12', 2, 5),
(53, '2022-01-04', 2, 5),
(54, '2022-01-12', 2, 5),
(55, '2022-02-02', 2, 5),
(56, '2022-02-09', 2, 5),
(57, '2022-02-02', 4, 5),
(58, '2022-02-16', 3, 4),
(59, '2022-02-10', 5, 3),
(60, '2022-02-18', 41, 5),
(61, '2022-02-10', 19, 4),
(62, '2022-03-09', 38, 5),
(63, '2022-03-28', 26, 4),
(64, '2022-03-23', 5, 5),
(66, '2022-03-25', 2, 5),
(67, '2022-03-23', 3, 3),
(68, '2022-01-11', 2, 5),
(69, '2022-01-20', 26, 5),
(70, '2022-01-27', 41, 1),
(71, '2022-04-13', 5, 5),
(72, '2022-04-20', 2, 3),
(73, '2022-06-14', 3, 4),
(74, '2022-04-21', 4, 5),
(75, '2022-06-23', 3, 5),
(76, '2022-06-15', 3, 3),
(77, '2022-06-30', 3, 4),
(78, '2022-07-14', 4, 3),
(79, '2022-07-19', 2, 5),
(80, '2022-07-07', 4, 5),
(81, '2022-07-08', 3, 5),
(82, '2022-07-28', 2, 4),
(83, '2022-09-15', 5, 1),
(84, '2022-09-28', 3, 5),
(85, '2022-09-06', 3, 5),
(86, '2022-09-07', 2, 5),
(87, '2022-09-15', 4, 5),
(88, '2022-09-08', 2, 5),
(89, '2022-09-13', 3, 4),
(90, '2022-09-22', 3, 4),
(91, '2022-09-07', 5, 4),
(92, '2022-12-27', 3, 5),
(93, '2022-12-15', 2, 5),
(94, '2022-10-01', 4, 5),
(95, '2022-10-03', 2, 4),
(96, '2022-10-04', 3, 5),
(97, '2022-07-13', 2, 5),
(98, '2022-07-20', 3, 5),
(99, '2022-02-10', 3, 5),
(100, '2022-02-23', 3, 1),
(101, '2022-06-15', 4, 5),
(102, '2022-09-14', 41, 5),
(103, '2022-08-11', 3, 5);

-- 
-- Dumping data for table productos_compras
--
INSERT INTO productos_compras VALUES
(2, '2022-08-17', 2, 3),
(3, '2022-08-17', 3, 5),
(4, '2022-08-18', 2, 4),
(5, '2022-08-18', 1, 1),
(7, '2022-08-26', 3, 5),
(8, '2022-08-31', 1, 1),
(9, '2022-08-31', 1, 1),
(10, '2022-08-31', 2, 1),
(11, '2022-08-31', 1, 1),
(12, '2022-08-31', 2, 1),
(13, '2022-08-31', 1, 1),
(14, '2022-08-31', 1, 1),
(17, '2022-08-31', 2, 1),
(18, '2022-08-31', 2, 1),
(19, '2022-09-03', 3, 3),
(20, '2022-09-04', 2, 3),
(21, '2022-01-04', 4, 2),
(22, '2022-01-17', 4, 2),
(23, '2022-02-17', 4, 2),
(24, '2022-03-02', 3, 2),
(25, '2022-02-24', 4, 3),
(26, '2022-03-18', 5, 3),
(27, '2022-03-30', 4, 3),
(28, '2022-04-04', 5, 2),
(29, '2022-03-21', 4, 2),
(30, '2022-09-20', 2, 3),
(31, '2022-09-20', 2, 3),
(32, '2022-04-14', 5, 2),
(33, '2022-04-26', 2, 2),
(34, '2022-09-15', 3, 4),
(35, '2022-09-22', 3, 3),
(36, '2022-09-06', 4, 2),
(37, '2022-01-05', 4, 2),
(38, '2022-01-20', 3, 4),
(39, '2022-01-26', 2, 3),
(40, '2022-07-19', 3, 2),
(41, '2022-06-22', 5, 4),
(42, '2022-01-21', 4, 2),
(43, '2022-01-18', 2, 2),
(44, '2022-01-13', 1, 2),
(45, '2022-06-13', 4, 2),
(46, '2022-06-22', 5, 2),
(47, '2022-06-28', 5, 1),
(48, '2022-06-01', 4, 1),
(49, '2022-07-11', 2, 2),
(50, '2022-04-13', 5, 2),
(51, '2022-02-08', 4, 2),
(52, '2022-02-21', 5, 1),
(53, '2022-02-08', 5, 1),
(54, '2022-02-24', 5, 1),
(55, '2022-02-28', 5, 1),
(56, '2022-02-24', 5, 2),
(57, '2022-02-10', 5, 2),
(58, '2022-02-17', 4, 2),
(59, '2022-06-20', 4, 2),
(60, '2022-06-08', 4, 2),
(61, '2022-06-15', 5, 2),
(62, '2022-06-23', 3, 1),
(63, '2022-06-28', 5, 3),
(64, '2022-02-09', 4, 2),
(65, '2022-02-16', 5, 3),
(66, '2022-02-08', 5, 2),
(67, '2022-05-12', 5, 2),
(68, '2022-06-13', 4, 2),
(69, '2022-09-14', 4, 3),
(70, '2022-07-13', 5, 3),
(71, '2022-07-13', 5, 2),
(72, '2022-07-28', 5, 1),
(73, '2022-04-05', 4, 1),
(74, '2022-04-19', 5, 1),
(75, '2022-07-21', 5, 2),
(76, '2022-07-05', 5, 2),
(77, '2022-07-13', 1, 2),
(78, '2022-07-28', 5, 2),
(79, '2022-07-20', 3, 1),
(80, '2022-10-03', 5, 1),
(81, '2022-10-10', 5, 2),
(82, '2022-10-10', 5, 1),
(83, '2022-10-25', 3, 2),
(84, '2022-11-22', 4, 2),
(85, '2022-10-18', 4, 2),
(86, '2022-12-05', 4, 2),
(87, '2022-10-17', 3, 1),
(88, '2022-12-21', 5, 2),
(89, '2022-12-29', 5, 1),
(90, '2022-12-27', 4, 4),
(91, '2022-12-28', 4, 2),
(92, '2022-12-14', 4, 5),
(93, '2022-12-22', 3, 5),
(94, '2022-01-20', 5, 2),
(95, '2022-01-19', 5, 2),
(96, '2022-01-12', 4, 5),
(97, '2022-07-01', 1, 3),
(98, '2022-09-27', 4, 3);

-- 
-- Dumping data for table citas
--
INSERT INTO citas VALUES
(3, '2022-08-17', '19:53:24', 3, 2),
(4, '2022-08-18', '11:48:49', 1, 4),
(5, '2022-08-18', '14:41:37', 4, 3),
(6, '2022-08-25', '10:07:15', 3, 5),
(8, '2022-08-25', '10:50:15', 3, 5),
(9, '2022-08-25', '11:50:15', 3, 3),
(10, '2022-08-26', '10:30:30', 3, 4),
(11, '2022-08-26', '10:30:31', 2, 1),
(12, '2022-08-26', '10:40:31', 5, 2),
(13, '2022-08-28', '11:50:15', 20, 3),
(14, '2022-08-28', '10:30:31', 4, 4),
(15, '2022-08-29', '10:50:15', 4, 5),
(17, '2022-08-29', '16:00:30', 5, 5),
(18, '2022-01-17', '10:07:13', 1, 2),
(19, '2022-02-17', '10:07:13', 1, 2),
(20, '2022-03-17', '12:07:13', 1, 4),
(21, '2022-04-17', '13:07:13', 1, 4),
(22, '2022-05-17', '11:07:13', 1, 3),
(23, '2022-06-17', '15:07:13', 1, 1),
(24, '2022-07-17', '16:07:13', 1, 4),
(25, '2022-09-17', '17:07:13', 1, 3),
(26, '2022-10-17', '14:07:13', 1, 3),
(27, '2022-11-17', '13:07:13', 1, 2),
(28, '2022-12-17', '10:07:13', 32, 2),
(29, '2022-08-30', '10:30:45', 8, 1),
(30, '2022-08-30', '11:00:00', 1, 1),
(31, '2022-08-30', '16:00:15', 4, 5),
(32, '2022-08-30', '17:30:30', 5, 1),
(33, '2022-08-30', '18:00:00', 21, 6),
(34, '2022-08-30', '19:00:30', 4, 6),
(35, '2022-08-30', '19:30:45', 20, 4),
(36, '2022-08-31', '18:00:30', 4, 5),
(37, '2022-08-31', '17:30:00', 5, 5),
(38, '2022-09-01', '16:00:00', 4, 5),
(39, '2022-09-02', '13:00:00', 4, 5),
(40, '2022-09-02', '19:00:00', 6, 5),
(41, '2022-08-26', '19:30:00', 5, 5),
(42, '2022-09-02', '19:30:00', 5, 6),
(43, '2022-09-02', '19:30:00', 5, 6),
(44, '2022-09-02', '20:30:00', 4, 5),
(45, '2022-09-03', '10:00:00', 5, 5),
(46, '2022-09-03', '10:30:00', 35, 1),
(47, '2022-08-31', '16:00:00', 5, 5),
(48, '2022-09-03', '17:30:00', 2, 1),
(49, '2022-09-03', '19:30:00', 5, 4),
(50, '2022-09-03', '20:30:00', 1, 2),
(51, '2022-09-03', '20:30:00', 6, 1),
(52, '2022-08-31', '21:00:00', 5, 5),
(53, '2022-09-04', '12:00:00', 20, 4),
(54, '2022-09-05', '11:00:00', 3, 4),
(55, '2022-09-05', '14:00:00', 1, 5),
(56, '2022-01-04', '17:00:00', 1, 2),
(57, '2022-02-02', '10:00:00', 2, 3),
(58, '2022-03-01', '09:00:00', 4, 4),
(59, '2022-05-26', '15:00:00', 38, 1),
(60, '2022-06-21', '17:00:00', 3, 2),
(61, '2022-06-18', '12:00:00', 27, 1),
(62, '2022-06-22', '17:00:00', 5, 1),
(63, '2022-07-18', '17:00:00', 3, 4),
(64, '2022-08-03', '17:00:00', 4, 4),
(65, '2022-09-09', '17:00:00', 5, 4),
(66, '2022-09-23', '17:00:00', 5, 4),
(67, '2022-10-13', '17:00:00', 4, 3),
(68, '2022-10-19', '17:00:00', 5, 5),
(69, '2022-12-24', '14:00:00', 4, 4),
(70, '2022-01-12', '17:00:00', 5, 5),
(71, '2022-01-12', '10:00:00', NULL, 4),
(72, '2022-01-18', '12:00:00', 4, 5),
(73, '2022-01-03', '17:18:00', 39, 4),
(74, '2022-02-02', '14:00:00', 2, 5),
(75, '2022-03-07', '12:45:00', 4, 4),
(76, '2022-03-29', '19:30:00', 1, 1),
(77, '2022-04-04', '10:30:00', 2, 1),
(78, '2022-05-09', '11:30:00', 38, 6),
(79, '2022-05-30', '13:00:00', 41, 1),
(80, '2022-07-05', '20:00:00', NULL, NULL),
(81, '2022-07-05', '20:00:00', 4, 6),
(82, '2022-06-13', '10:00:00', 4, 3),
(83, '2022-06-30', '12:30:00', 28, 3),
(84, '2022-09-20', '16:30:00', 2, 3),
(85, '2022-10-10', '17:30:00', 4, 3),
(86, '2022-10-17', '18:30:00', 9, 1),
(87, '2022-09-26', '17:30:00', 4, 5),
(88, '2022-11-07', '17:30:00', 5, 5),
(89, '2022-10-19', '11:30:00', 3, 1),
(90, '2022-11-23', '17:30:00', 6, 1),
(91, '2022-11-18', '17:30:00', 1, 1),
(92, '2022-11-24', '09:30:00', 6, 4),
(93, '2022-12-05', '17:30:00', 5, 3),
(94, '2022-12-13', '17:30:00', 3, 3),
(95, '2022-12-21', '17:30:00', 4, 4);

--
-- Set default database
--
USE managebarbershopv3;

DELIMITER $$

--
-- Create trigger `restarC`
--
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER restarC
AFTER INSERT
ON productos_ventas
FOR EACH ROW
BEGIN
  UPDATE productos
  SET cantidad = cantidad - 1
  WHERE IDproducto = new.IDproducto_venta_producto;
END
$$

--
-- Create trigger `sumarC`
--
CREATE OR REPLACE
DEFINER = 'root'@'localhost'
TRIGGER sumarC
AFTER INSERT
ON productos_compras
FOR EACH ROW
BEGIN
  UPDATE productos
  SET cantidad = cantidad + 1
  WHERE IDproducto = new.IDproducto_compra_producto;
END
$$

DELIMITER ;

-- 
-- Restore previous SQL mode
-- 
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;

-- 
-- Enable foreign keys
-- 
/*!40014 SET FOREIGN_KEY_CHECKS = @OLD_FOREIGN_KEY_CHECKS */;