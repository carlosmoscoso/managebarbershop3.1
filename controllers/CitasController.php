<?php

namespace app\controllers;

use app\models\Citas;
use app\models\Clientes;
use app\models\Servicios;
use app\models\CitasSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use yii\data\ActiveDataProvider;
use yii\db\Query; 
/**
 * CitasController implements the CRUD actions for Citas model.
 */
class CitasController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Citas models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new CitasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    //Mostrar las citas según las fechas
    
    public function actionTotal()
    {
        $searchModel = new CitasSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('total', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionEnero()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-01-01' AS date ) AND fecha <= CAST('2022-01-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
        $totalCitas = Citas::find()->where("fecha >= CAST('2022-01-01' AS date ) AND fecha <= CAST('2022-01-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
        
        $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-01-01' AS date ) AND c.fecha <= CAST('2022-01-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('enero', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    
    
    public function actionFebrero()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-02-01' AS date ) AND fecha <= CAST('2022-02-27' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-02-01' AS date ) AND fecha <= CAST('2022-02-27' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-02-01' AS date ) AND c.fecha <= CAST('2022-02-27' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('febrero', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    
    public function actionMarzo()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-03-01' AS date ) AND fecha <= CAST('2022-03-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
              $totalCitas = Citas::find()->where("fecha >= CAST('2022-03-01' AS date ) AND fecha <= CAST('2022-03-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-03-01' AS date ) AND c.fecha <= CAST('2022-03-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        
        return $this->render('marzo', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionAbril()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-04-01' AS date ) AND fecha <= CAST('2022-04-30' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-04-01' AS date ) AND fecha <= CAST('2022-04-30' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-04-01' AS date ) AND c.fecha <= CAST('2022-04-30' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        
        return $this->render('abril', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionMayo()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-05-01' AS date ) AND fecha <= CAST('2022-05-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-05-01' AS date ) AND fecha <= CAST('2022-05-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-05-01' AS date ) AND c.fecha <= CAST('2022-05-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('mayo', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionJunio()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-06-01' AS date ) AND fecha <= CAST('2022-06-30' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-06-01' AS date ) AND fecha <= CAST('2022-06-30' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-06-01' AS date ) AND c.fecha <= CAST('2022-06-30' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('junio', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionJulio()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-07-01' AS date ) AND fecha <= CAST('2022-07-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-07-01' AS date ) AND fecha <= CAST('2022-07-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-07-01' AS date ) AND c.fecha <= CAST('2022-07-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        
        return $this->render('julio', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    
    
    public function actionAgosto()
    {
        $fecha = date('Y-m-d');
       // $clientes = Clientes::find();
       // $servicios = Servicios::find();
        
        $fechaCondicion = "fecha > CAST('2022-08-01' AS date ) AND fecha < CAST('2022-08-31' AS date )";
        
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where($fechaCondicion)->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        $totalCitas = Citas::find()->where($fechaCondicion)->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
        
        
        
       $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-08-01' AS date ) AND c.fecha <= CAST('2022-08-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('agosto', [
                    'dp' => $dataProvider,
                    //"totalBeneficio"=>$dataProvider2,
                    //"campos"=>['IDcitas'],
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
        ]);
    }
    
    public function actionSeptiembre()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-09-01' AS date ) AND fecha <= CAST('2022-09-30' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-09-01' AS date ) AND fecha <= CAST('2022-09-30' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-09-01' AS date ) AND c.fecha <= CAST('2022-09-30' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('septiembre', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionOctubre()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-10-01' AS date ) AND fecha <= CAST('2022-10-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-10-01' AS date ) AND fecha <= CAST('2022-10-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-10-01' AS date ) AND c.fecha <= CAST('2022-10-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('octubre', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionNoviembre()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-11-01' AS date ) AND fecha <= CAST('2022-11-30' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-11-01' AS date ) AND fecha <= CAST('2022-11-30' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-11-01' AS date ) AND c.fecha <= CAST('2022-11-30' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        
        return $this->render('noviembre', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    public function actionDiciembre()
    {
        $fecha = date('Y-m-d');
        $dataProvider = new ActiveDataProvider([
                'query' => Citas::find()->where("fecha >= CAST('2022-12-01' AS date ) AND fecha <= CAST('2022-12-31' AS date )")->orderBy('hora'),
                'pagination' =>['pageSize'=>8],
            ]);
        
               $totalCitas = Citas::find()->where("fecha >= CAST('2022-12-01' AS date ) AND fecha <= CAST('2022-12-31' AS date )")->count();
        
       // $totalBeneficio = Citas::find()->where($fechaCondicion)->joinWith($servicios)->select
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        $insql = $connection->createCommand("SELECT SUM(s.coste) AS total FROM citas c
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
       WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) ");  
        $totalBeneficio=$insql->queryColumn();
	   
	   $insql2 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'corte pelo'");  
         $totalServiciosPelo=$insql2->queryOne();
       
       $insql3 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'Barba'");  
         $totalServiciosBarba=$insql3->queryOne();
         
        $insql4 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'tinte de barba'");  
         $totalServiciosTinteBarba=$insql4->queryOne(); 
         
        $insql5 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'Tinte de pelo'");  
         $totalServiciosTintePelo=$insql5->queryOne();
         
         $insql6 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'Corte pelo niño'");  
         $totalServiciosPeloNiño=$insql6->queryOne();
         
       $insql7 = $connection->createCommand("SELECT COUNT(*) AS total FROM citas c     
        LEFT JOIN servicios s ON c.IDcitas_servicio = s.IDservicio
        WHERE c.fecha >= CAST('2022-12-01' AS date ) AND c.fecha <= CAST('2022-12-31' AS date ) AND s.nombre like 'Cejas'");  
         $totalServiciosCejas=$insql7->queryOne();
        
        return $this->render('diciembre', [
                    'dp' => $dataProvider,
                    'totalCitas' => $totalCitas,
                    'totalBeneficio' => $totalBeneficio,           
                    'totalServiciosPelo' => $totalServiciosPelo, 
                    'totalServiciosBarba' => $totalServiciosBarba,
                    'totalServiciosTinteBarba' => $totalServiciosTinteBarba,
                    'totalServiciosTintePelo' => $totalServiciosTintePelo,
                    'totalServiciosPeloNiño' => $totalServiciosPeloNiño,
                    'totalServiciosCejas' => $totalServiciosCejas,
            
        ]);
    }
    
    // datos de tarjetas para ver cuantos clientes y servicios a tenido por mes

    /**
     * Displays a single Citas model.
     * @param int $IDcitas I Dcitas
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDcitas)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDcitas),
        ]);
    }

    /**
     * Creates a new Citas model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Citas();
        
        $cliente = Clientes::find()->all();
        $servicio = Servicios::find()->all();
        
        $listaCliente = ArrayHelper::map($cliente, 'IDcliente', 'nombre');
        $listaServicio = ArrayHelper::map($servicio, 'IDservicio', 'nombre');

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDcitas' => $model->IDcitas]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'listaCliente' => $listaCliente,
            'listaServicio' => $listaServicio
        ]);
    }

    /**
     * Updates an existing Citas model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDcitas I Dcitas
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDcitas)
    {
        $model = $this->findModel($IDcitas);
        
        $cliente = Clientes::find()->all();
        $servicio = Servicios::find()->all();
        
        $listaCliente = ArrayHelper::map($cliente, 'IDcliente', 'nombre');
        $listaServicio = ArrayHelper::map($servicio, 'IDservicio', 'nombre');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDcitas' => $model->IDcitas]);
        }

        return $this->render('update', [
            'model' => $model,
            'listaCliente' => $listaCliente,
            'listaServicio' => $listaServicio
                
        ]);
    }

    /**
     * Deletes an existing Citas model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDcitas I Dcitas
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDcitas)
    {
        $this->findModel($IDcitas)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Citas model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDcitas I Dcitas
     * @return Citas the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDcitas)
    {
        if (($model = Citas::findOne(['IDcitas' => $IDcitas])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
