<?php

namespace app\controllers;

use app\models\Telefonos;
use app\models\TelefonosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Clientes;
use yii\data\ActiveDataProvider;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;

/**
 * TelefonosController implements the CRUD actions for Telefonos model.
 */
class TelefonosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Telefonos models.
     *
     * @return string
     */
    
    public function actionIndex()
    {
        $searchModel = new TelefonosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Telefonos model.
     * @param int $IDtelefono I Dtelefono
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDtelefono)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDtelefono),
        ]);
    }

    /**
     * Creates a new Telefonos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
            
    
    
    public function actionCreate($IDcliente)
    {
        $model = new Telefonos();      
        $cliente = Clientes::find()->where("IDcliente = . $IDcliente" );
             
        $listaCliente = ArrayHelper::map($cliente, 'IDcliente', 'nombre');
       

        if ($this->request->isPost) {
            $model->load($this->request->post());
            $model->IDtelefono_cliente = $IDcliente;
            if ($model->save()) {
                return $this->redirect(['view', 'IDtelefono' => $model->IDtelefono]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'listaCliente' => $listaCliente,
            
        ]);
    }


    /**
     * Updates an existing Telefonos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDtelefono I Dtelefono
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDtelefono)
    {
        $model = $this->findModel($IDtelefono);
        //$cliente = Clientes::find()->all();
        //$Aclientes = new Clientes();
        
        //$listaCliente = ArrayHelper::map($cliente, 'IDcliente', 'nombre');

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDtelefono' => $model->IDtelefono]);
        }

        return $this->render('update', [
            'model' => $model,
             //'listaCliente' => $listaCliente,
           // 'Aclientes' => $Aclientes,
            
        ]);
    }

    /**
     * Deletes an existing Telefonos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDtelefono I Dtelefono
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDtelefono)
    {
        $this->findModel($IDtelefono)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Telefonos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDtelefono I Dtelefono
     * @return Telefonos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDtelefono)
    {
        if (($model = Telefonos::findOne(['IDtelefono' => $IDtelefono])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
