<?php

namespace app\controllers;

use app\models\Servicios;
use app\models\ServiciosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;

/**
 * ServiciosController implements the CRUD actions for Servicios model.
 */
class ServiciosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Servicios models.
     *
     * @return string
     */
   public function actionModificar()
    {
        $searchModel = new ServiciosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('modificar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndex()
   {
        
        $dataProvider = new ActiveDataProvider([
                'query' => Servicios::find(),
                'pagination' =>['pageSize'=>8],
            ]);
        
        return $this->render('index', [
                    'dp' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Servicios model.
     * @param int $IDservicio I Dservicio
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDservicio)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDservicio),
        ]);
    }

    /**
     * Creates a new Servicios model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Servicios();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDservicio' => $model->IDservicio]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Servicios model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDservicio I Dservicio
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDservicio)
    {
        $model = $this->findModel($IDservicio);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDservicio' => $model->IDservicio]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Servicios model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDservicio I Dservicio
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDservicio)
    {
        $this->findModel($IDservicio)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Servicios model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDservicio I Dservicio
     * @return Servicios the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDservicio)
    {
        if (($model = Servicios::findOne(['IDservicio' => $IDservicio])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
