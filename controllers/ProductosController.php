<?php

namespace app\controllers;

use app\models\Productos;
use app\models\ProductosSearch;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\data\ActiveDataProvider;
use yii\db\Query; 

/**
 * ProductosController implements the CRUD actions for Productos model.
 */
class ProductosController extends Controller
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Productos models.
     *
     * @return string
     */
    public function actionModificar()
    {
        $searchModel = new ProductosSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('modificar', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }
    
    public function actionIndex()
   {
        
        $dataProvider = new ActiveDataProvider([
                'query' => Productos::find(),
                'pagination' =>['pageSize'=>5],
            ]);
        
        
        $connection = \Yii::$app->db; 
        $query = new Query; 
        
        //ENERO
        $insqlE = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-01-01' AS date ) AND pc.fecha <= CAST('2022-01-31' AS date ) ");
         $totalGastoProductosEne=$insqlE->queryColumn();
        
      $insqlE2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-01-01' AS date ) AND pv.fecha <= CAST('2022-01-30' AS date ) ");
        $totalBeneficioProductosEne=$insqlE2->queryColumn();
        
      $insqlE3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-01-01' AS date ) AND pv.fecha <= CAST('2022-01-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-01-01' AS date ) AND pc.fecha <= CAST('2022-01-31' AS date ))  ");
        $totalBeneficioNetoProductosEne=$insqlE3->queryColumn();
        
        
       //FEBRERO
        $insqlF = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-02-01' AS date ) AND pc.fecha <= CAST('2022-02-27' AS date )");
         $totalGastoProductosFeb=$insqlF->queryColumn();
        
      $insqlF2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-02-01' AS date ) AND pv.fecha <= CAST('2022-02-27' AS date ) ");
        $totalBeneficioProductosFeb=$insqlF2->queryColumn();
        
      $insqlFe3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-02-01' AS date ) AND pv.fecha <= CAST('2022-02-27' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-02-01' AS date ) AND pc.fecha <= CAST('2022-02-27' AS date )) ");
        $totalBeneficioNetoProductosFeb=$insqlFe3->queryColumn();
        
        //MARZO
        $insqlM = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-03-01' AS date ) AND pc.fecha <= CAST('2022-03-31' AS date ) ");
         $totalGastoProductosMar=$insqlM->queryColumn();
        
      $insqlM2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-03-01' AS date ) AND pv.fecha <= CAST('2022-03-31' AS date ) ");
        $totalBeneficioProductosMar=$insqlM2->queryColumn();
        
      $insqlM3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-03-01' AS date ) AND pv.fecha <= CAST('2022-03-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-03-01' AS date ) AND pc.fecha <= CAST('2022-03-31' AS date )) ");
        $totalBeneficioNetoProductosMar=$insqlM3->queryColumn();
        
        //ABRIL
        $insqlA = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-04-01' AS date ) AND pc.fecha <= CAST('2022-04-30' AS date ) ");
         $totalGastoProductosAbr=$insqlA->queryColumn();
        
      $insqlA2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-04-01' AS date ) AND pv.fecha <= CAST('2022-04-30' AS date ) ");
        $totalBeneficioProductosAbr=$insqlA2->queryColumn();
        
      $insqlA3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-04-01' AS date ) AND pv.fecha <= CAST('2022-04-30' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-04-01' AS date ) AND pc.fecha <= CAST('2022-04-30' AS date )) ");
        $totalBeneficioNetoProductosAbr=$insqlA3->queryColumn();
        
        //MAYO
        $insqlMA = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-05-01' AS date ) AND pc.fecha <= CAST('2022-05-31' AS date ) ");
         $totalGastoProductosMay=$insqlMA->queryColumn();
        
      $insqlMA2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-05-01' AS date ) AND pv.fecha <= CAST('2022-05-31' AS date )");
        $totalBeneficioProductosMay=$insqlMA2->queryColumn();
        
      $insqlMA3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-05-01' AS date ) AND pv.fecha <= CAST('2022-05-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-05-01' AS date ) AND pc.fecha <= CAST('2022-05-31' AS date ))  ");
        $totalBeneficioNetoProductosMay=$insqlMA3->queryColumn();
        
        //JUNIO
        $insqlJUN = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-06-01' AS date ) AND pc.fecha <= CAST('2022-06-30' AS date ) ");
         $totalGastoProductosJun=$insqlJUN->queryColumn();
        
      $insqlJUN2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-06-01' AS date ) AND pv.fecha <= CAST('2022-06-30' AS date ) ");
        $totalBeneficioProductosJun=$insqlJUN2->queryColumn();
        
      $insqlJUN3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-06-01' AS date ) AND pv.fecha <= CAST('2022-06-30' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-06-01' AS date ) AND pc.fecha <= CAST('2022-06-30' AS date ))  ");
        $totalBeneficioNetoProductosJun=$insqlJUN3->queryColumn();
        
        //JULIO
        $insqlJUL = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-07-01' AS date ) AND pc.fecha <= CAST('2022-07-31' AS date ) ");
         $totalGastoProductosJul=$insqlJUL->queryColumn();
        
      $insqlJUL2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-07-01' AS date ) AND pv.fecha <= CAST('2022-07-31' AS date )");
        $totalBeneficioProductosJul=$insqlJUL2->queryColumn();
        
      $insqlJUL3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-07-01' AS date ) AND pv.fecha <= CAST('2022-07-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-07-01' AS date ) AND pc.fecha <= CAST('2022-07-31' AS date ))  ");
        $totalBeneficioNetoProductosJul=$insqlJUL3->queryColumn();
        
        //AGOSTO
        $insqlAGO = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-08-01' AS date ) AND pc.fecha <= CAST('2022-08-31' AS date ) ");
         $totalGastoProductosAgo=$insqlAGO->queryColumn();
        
      $insqlAGO2 = $connection->createCommand("      SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-08-01' AS date ) AND pv.fecha <= CAST('2022-08-31' AS date )");
        $totalBeneficioProductosAgo=$insqlAGO2->queryColumn();
        
      $insqlAGO3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-08-01' AS date ) AND pv.fecha <= CAST('2022-08-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-08-01' AS date ) AND pc.fecha <= CAST('2022-08-31' AS date ))  ");
        $totalBeneficioNetoProductosAgo=$insqlAGO3->queryColumn();
        
        //SEPTIEMBRE
        $insqlSEP = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-09-01' AS date ) AND pc.fecha <= CAST('2022-09-30' AS date ) ");
         $totalGastoProductosSep=$insqlSEP->queryColumn();
        
      $insqlSEP2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-09-01' AS date ) AND pv.fecha <= CAST('2022-09-30' AS date )");
        $totalBeneficioProductosSep=$insqlSEP2->queryColumn();
        
      $insqlSEP3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-09-01' AS date ) AND pv.fecha <= CAST('2022-09-30' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-09-01' AS date ) AND pc.fecha <= CAST('2022-09-30' AS date ))  ");
        $totalBeneficioNetoProductosSep=$insqlSEP3->queryColumn();
        
        //OCTUBRE
        $insqlOCT = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-10-01' AS date ) AND pc.fecha <= CAST('2022-10-31' AS date ) ");
         $totalGastoProductosOct=$insqlOCT->queryColumn();
        
      $insqlOCT2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-10-01' AS date ) AND pv.fecha <= CAST('2022-10-31' AS date ) ");
        $totalBeneficioProductosOct=$insqlOCT2->queryColumn();
        
      $insqlOCT3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-10-01' AS date ) AND pv.fecha <= CAST('2022-10-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-10-01' AS date ) AND pc.fecha <= CAST('2022-10-31' AS date ))  ");
        $totalBeneficioNetoProductosOct=$insqlOCT3->queryColumn();
        
        
        //NOVIEMBRE
        $insqlNOV = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-11-01' AS date ) AND pc.fecha <= CAST('2022-11-30' AS date ) ");
         $totalGastoProductosNov=$insqlNOV->queryColumn();
        
      $insqlNOV2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-11-01' AS date ) AND pv.fecha <= CAST('2022-11-30' AS date ) ");
        $totalBeneficioProductosNov=$insqlNOV2->queryColumn();
        
      $insqlNOV3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-11-01' AS date ) AND pv.fecha <= CAST('2022-11-30' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-11-01' AS date ) AND pc.fecha <= CAST('2022-11-30' AS date ))  ");
        $totalBeneficioNetoProductosNov=$insqlNOV3->queryColumn();
        
        //DICIEMBRE
        $insqlDIC = $connection->createCommand("SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-12-01' AS date ) AND pc.fecha <= CAST('2022-12-31' AS date ) ");
         $totalGastoProductosDic=$insqlDIC->queryColumn();
        
      $insqlDIC2 = $connection->createCommand("SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-12-01' AS date ) AND pv.fecha <= CAST('2022-12-31' AS date ) ");
        $totalBeneficioProductosDic=$insqlDIC2->queryColumn();
        
      $insqlDIC3 = $connection->createCommand("SELECT (SELECT SUM(p.costeCliente) AS total FROM productos_ventas pv
        LEFT outer JOIN productos p ON pv.IDproducto_venta_producto = p.IDproducto
      WHERE pv.fecha >= CAST('2022-12-01' AS date ) AND pv.fecha <= CAST('2022-12-31' AS date )) - (SELECT SUM(p.costeProveedores) AS total FROM productos_compras pc
      LEFT OUTER JOIN productos p ON pc.IDproducto_compra_producto = p.IDproducto
      WHERE  pc.fecha >= CAST('2022-12-01' AS date ) AND pc.fecha <= CAST('2022-12-31' AS date ))  ");
        $totalBeneficioNetoProductosDic=$insqlDIC3->queryColumn();
       
        
        return $this->render('index', [
                    'dp' => $dataProvider,
            
                    'totalGastoProductosEne' => $totalGastoProductosEne,
                    'totalBeneficioProductosEne' => $totalBeneficioProductosEne,
                    'totalBeneficioNetoProductosEne' => $totalBeneficioNetoProductosEne,  
            
                    'totalGastoProductosFeb' => $totalGastoProductosFeb,
                    'totalBeneficioProductosFeb' => $totalBeneficioProductosFeb,
                    'totalBeneficioNetoProductosFeb' => $totalBeneficioNetoProductosFeb,
            
                    'totalGastoProductosMar' => $totalGastoProductosMar,
                    'totalBeneficioProductosMar' => $totalBeneficioProductosMar,
                    'totalBeneficioNetoProductosMar' => $totalBeneficioNetoProductosMar,
            
                    'totalGastoProductosAbr' => $totalGastoProductosAbr,
                    'totalBeneficioProductosAbr' => $totalBeneficioProductosAbr,
                    'totalBeneficioNetoProductosAbr' => $totalBeneficioNetoProductosAbr,
            
                    'totalGastoProductosMay' => $totalGastoProductosMay,
                    'totalBeneficioProductosMay' => $totalBeneficioProductosMay,
                    'totalBeneficioNetoProductosMay' => $totalBeneficioNetoProductosMay,
            
                    'totalGastoProductosJun' => $totalGastoProductosJun,
                    'totalBeneficioProductosJun' => $totalBeneficioProductosJun,
                    'totalBeneficioNetoProductosJun' => $totalBeneficioNetoProductosJun,
            
                    'totalGastoProductosJul' => $totalGastoProductosJul,
                    'totalBeneficioProductosJul' => $totalBeneficioProductosJul,
                    'totalBeneficioNetoProductosJul' => $totalBeneficioNetoProductosJul,

                    'totalGastoProductosAgo' => $totalGastoProductosAgo,
                    'totalBeneficioProductosAgo' => $totalBeneficioProductosAgo,
                    'totalBeneficioNetoProductosAgo' => $totalBeneficioNetoProductosAgo,
            
                    'totalGastoProductosSep' => $totalGastoProductosSep, 
                    'totalBeneficioProductosSep' => $totalBeneficioProductosSep,
                    'totalBeneficioNetoProductosSep' => $totalBeneficioNetoProductosSep,
            
                    'totalGastoProductosOct' => $totalGastoProductosOct,
                    'totalBeneficioProductosOct' => $totalBeneficioProductosOct,
                    'totalBeneficioNetoProductosOct' => $totalBeneficioNetoProductosOct,
            
                    'totalGastoProductosNov' => $totalGastoProductosNov,
                    'totalBeneficioProductosNov' => $totalBeneficioProductosNov,
                    'totalBeneficioNetoProductosNov' => $totalBeneficioNetoProductosNov,
            
                    'totalGastoProductosDic' => $totalGastoProductosDic,
                    'totalBeneficioProductosDic' => $totalBeneficioProductosDic,
                    'totalBeneficioNetoProductosDic' => $totalBeneficioNetoProductosDic,
            
                    
            
            
        ]);
    }

    /**
     * Displays a single Productos model.
     * @param int $IDproducto I Dproducto
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDproducto)
    {
        return $this->render('view', [
            'model' => $this->findModel($IDproducto),
        ]);
    }

    /**
     * Creates a new Productos model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Productos();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'IDproducto' => $model->IDproducto]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Productos model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDproducto I Dproducto
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDproducto)
    {
        $model = $this->findModel($IDproducto);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'IDproducto' => $model->IDproducto]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Productos model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDproducto I Dproducto
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDproducto)
    {
        $this->findModel($IDproducto)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Productos model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDproducto I Dproducto
     * @return Productos the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDproducto)
    {
        if (($model = Productos::findOne(['IDproducto' => $IDproducto])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
