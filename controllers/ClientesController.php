<?php

namespace app\controllers;

use app\models\Clientes;
use app\models\ClientesSearch;
use app\models\Telefonos;
//use app\models\TelefonosSearch;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\helpers\ArrayHelper;

/**
 * ClientesController implements the CRUD actions for Clientes model.
 */
class ClientesController extends Controller {

    /**
     * @inheritDoc
     */
    public function behaviors() {
        return array_merge(
                parent::behaviors(),
                [
                    'verbs' => [
                        'class' => VerbFilter::className(),
                        'actions' => [
                            'delete' => ['POST'],
                        ],
                    ],
                ]
        );
    }

    /**
     * Lists all Clientes models.
     *
     * @return string
     */
    public function actionIndex() {

        $dataProvider = new ActiveDataProvider([
            'query' => Clientes::find(),
            'pagination' => ['pageSize' => 8],
        ]);

        return $this->render('index', [
                    'dp' => $dataProvider,
        ]);
    }

    public function actionModificar() {
        $searchModel = new ClientesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

//        $searchModelTlf = new TelefonosSearch();
//        $dataProvider2 = $searchModelTlf->search($this->request->queryParams);
        
//         $telefono = Telefonos::find()->all();
//          $listaTelefono = ArrayHelper::map($telefono, 'IDtelefono', 'telefono1','telefono2');

        return $this->render('modificar', [
                    'searchModel' => $searchModel,
                    'dataProvider' => $dataProvider,
//                    'listaTelefono' => $listaTelefono,
//                    'searchModelTlf' => $searchModelTlf,
//                    'dataProvider2' => $dataProvider2,
        ]);
    }

    /**
     * Displays a single Clientes model.
     * @param int $IDcliente I Dcliente
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($IDcliente) {
        return $this->render('view', [
                    'model' => $this->findModel($IDcliente),
        ]);
    }

    /**
     * Creates a new Clientes model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate() {
        $model = new Clientes();

        if ($this->request->isPost) {

            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['telefonos/create', 'IDcliente' => $model->IDcliente]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
                    'model' => $model,
        ]);
    }

    /**
     * Updates an existing Clientes model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $IDcliente I Dcliente
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($IDcliente) {
        $model = $this->findModel($IDcliente);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['telefonos/create', 'IDcliente' => $model->IDcliente]);
        }

        return $this->render('update', [
                    'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Clientes model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $IDcliente I Dcliente
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($IDcliente) {
        $this->findModel($IDcliente)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Clientes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $IDcliente I Dcliente
     * @return Clientes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($IDcliente) {
        if (($model = Clientes::findOne(['IDcliente' => $IDcliente])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }

}
