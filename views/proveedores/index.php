<?php


use circulon\widgets\ColumnListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Proveedores';
//$this->params['breadcrumbs'][] = $this->title;
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('PROVEEDORES','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>


<div class ="row justify-content-center">
        <div class = "card col-4 m-2 colorManager"> 
        <div class = "card-body">
    
               <h5 class="card-text">
                FECHA:<br> <?php $fechaActual = date('d-m-Y'); echo $fechaActual; ?> 
            </h5>
        </div>
    </div>
       
        <?= Html::button('Crear proveedor', ['value'=>Url::to(['proveedores/create']),'class'=>'centrar card-body card col-2 m-2 colorManager  zoom','id'=>'modalButton']) ?>
            <?php
                    Modal::begin([
                        'id'=>'modal',
                        'size'=>'50',
                    ]);
                    echo "<div id='modalContent'></div>";
                    Modal::end();
            ?>                     
        <?= Html::a('Modificar proveedor',['proveedores/modificar'],['class' => 'centrar card-body card col-2 m-2 colorManager zoom'])?>

        

  
    
</div>
  <br>
    <br>
<!--PRUEBA-->

    <div class="container ">
    <div class="row justify-content-center">
        <div class="col align-self-lg-start">
        <?=
        ColumnListView::widget([
            'pager' => [
                'class' => \yii\bootstrap4\LinkPager::class
            ],
            'dataProvider' => $dp,
            'itemView' => '_index', //tiene que llamarse como el fichero
            'layout' => "{items}\n{pager}",
             'columns'=> 2,
            
        ]);
        ?>

    </div> 
</div>
</div>
<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>