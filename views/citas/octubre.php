<?php

/** @var yii\web\View $this */

use circulon\widgets\ColumnListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

$this->title = 'ManagerBarberShop';
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('OCTUBRE','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
 <div class="centrar p-3">
        <?= Html::a('Volver',['citas/total'],['class' => 'centrar card-body card col-2 m-2 colorManager zoom'])?>                  
    </div>  
        <div class="col-12 centrar p-4">
            <h2 class="colorBarber">RESUMEN MENSUAL</h2>
             
        </div>
    
        <hr class="manager">
        
    <div class ="row justify-content-center "> 
        
    <div class =" card-body row justify-content-center col-12 ">
        
        <div class="col-12 centrar m-3">
            <h2 class="colorBarber">Servicios</h2>    
        </div>
        
        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE CORTE DE PELO </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosPelo); ?>  </p>
        </div>
        
        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE BARBA </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosBarba); ?>  </p>
        </div>
        
        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE TINTE BARBA </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosTinteBarba); ?>  </p>
        </div>

        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE TINTE PELO </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosTintePelo); ?>  </p>
        </div>
        
        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE CORTE DE NIÑO </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosPeloNiño); ?>  </p>
        </div>  
        
                
        <div class = "card card-body col-3 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE CEJAS </p>  
            <hr class="colorBarber">       
            <p> <?=  implode(" ", $totalServiciosCejas); ?>  </p>
        </div>
        
    </div>
        
         <div class =" card-body row justify-content-center col-12">
        <div class="col-12 centrar m-3">
            <h2 class="colorBarber">Ingresos y citas</h2>    
        </div>
        
        <div class = "card card-body col-4 m-2  zoom sinborde colorManager"> 

            <p>TOTAL CITAS DEL MES </p>  

            <hr class="colorBarber">
            <?= $totalCitas ?>
        </div>
    
        <div class = "card card-body col-4 m-2  zoom sinborde colorManager"> 
            <p>TOTAL DE INGRESO </p>  
            <hr class="colorBarber">
            <p> <?=  implode(" ", $totalBeneficio); ?> €</p>

        </div>

        
    </div>   
        
</div>
<br>
<br>
 <hr class="manager">

<div class="container">
<div class="row justify-content-center">
        <div class="col align-self-lg-start">
        <?=
        ColumnListView::widget([
            'pager' => [
                'class' => \yii\bootstrap4\LinkPager::class
            ],
            'dataProvider' => $dp,
            'itemView' => '_octubre', //tiene que llamarse como el fichero
            'layout' => "{items}\n{pager}",
             'columns'=> 2,
        ]);
        ?>

   </div> 
         </div> 
       </div> 


<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>
