<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;
use kartik\widgets\TimePicker;


/* @var $this yii\web\View */
/* @var $model app\models\Citas */
/* @var $model app\models\Clientes */
/* @var $model app\models\Servicios */
/* @var $form yii\widgets\ActiveForm */
?>
<header>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" crossorigin="anonymous"></script>
</header>
<body>

<div class="citas-form">
    <div class = "card col-12 colorManagersin text-white"> 

<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('CREAR CITA','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>

<br>
        <div class = "card-body">
          

   <?php $form = ActiveForm::begin(); ?>
            
   <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::classname(), [
            'type' => DatePicker::TYPE_COMPONENT_APPEND,
            'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
            'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
            'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>
            
    
            
    <?= $form->field($model, 'hora')->widget(kartik\time\TimePicker::className(), [
            'pluginOptions' => [
            'showSeconds' => false,
            'showMeridian' => false,
            'minuteStep' => 30,
            
            
    ]
        
    ]); ?>        
    
    
   <?= $form->field($model, 'IDcitas_cliente')->widget(Select2::classname(), [
                'data' => $listaCliente,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del cliente') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>
    
    <?= $form->field($model, 'IDcitas_servicio')->widget(Select2::classname(), [
                'data' => $listaServicio,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del servicio') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>
            <br>
    
    <div class="row justify-content-center">
        <?= Html::submitButton('CREAR', ['class' => 'centrar card-body card colorManager col-4 m-2 zoom sinborde']) ?>
         <?= Html::a('CREAR CLIENTE',['clientes/index'],['class' => 'centrar card-body card colorManager card col-4 m-2  zoom sinborde'])?>       
    </div>

    <?php ActiveForm::end(); ?>

        </div>

    </div>
</div>
</body>

