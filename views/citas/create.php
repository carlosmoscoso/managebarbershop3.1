<?php

use yii\helpers\Html;
?>
<div class="citas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listaCliente' => $listaCliente,
        'listaServicio' => $listaServicio 
    ]) ?>

</div>
