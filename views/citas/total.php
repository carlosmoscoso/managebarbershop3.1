<?php

/** @var yii\web\View $this */

use circulon\widgets\ColumnListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

$this->title = 'ManagerBarberShop';
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('HISTORIAL DE CITAS MENSUALES','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>

    
    <div class="row justify-content-center">
        <h4 class="card card-body m-2 colorBarber centrar sinborde"> A continuación se mostrará un calendario con los meses del año seleccione dando clic a cualquiera de los meses </h4> 
    </div>
    
    <div class ="row justify-content-center">
        <h2 class="card-body card col-3 m-2 colorManager "> 2022 </h2> 
    </div>
        
<br>
<br>
<div class="container ">
    <div class ="row justify-content-center ">

     
        <?= Html::a('Enero',['citas/enero'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Febrero',['citas/febrero'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Marzo',['citas/marzo'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Abril',['citas/abril'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Mayo',['citas/mayo'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Junio',['citas/junio'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Julio',['citas/julio'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Agosto',['citas/agosto'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Septiembre',['citas/septiembre'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Octubre',['citas/octubre'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Noviembre',['citas/noviembre'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
        <?= Html::a('Diciembre',['citas/diciembre'],['class' => 'card-body card col-3 m-2 colorManager zoom'])?>
    
    </div> 
</div>

<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>






