<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Servicios */

$this->title = $model->nombre;
//$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('SERVICIOS','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>

    
<div class="row justify-content-center">

  <div class="container">
      
      
      <div class="col-12 card card-body m-2">
          <h3 class="colorBarber centrar">Se actualizará el siguiente servicio: <?php echo $model->nombre ?></h3>
              <?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    'IDservicio',
                    'nombre',
                    'coste',
                ],
            ]) ?>
          
      </div>

    <div class="col-12 card card-body m-2">
        <div class="centrar p-12 m-2">
           
             <h3 class="colorBarber centrar">¿Quiere ACTUALIZAR o BORRAR o VOLVER?</h3>
        
        </div>
         <div class="centrar p-12 m-2">
            <p>
        <?= Html::a('Actualizar', ['update', 'IDservicio' => $model->IDservicio], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'IDservicio' => $model->IDservicio], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Está seguro de querer borrar este servicio?',
                'method' => 'post',
            ],
        ]) ?>
        
        <?= Html::a('Volver',['servicios/modificar'],['class' => ' btn  colorManagersin'])?>                  
    
            </p>
         </div>    
         </div>
    
    </div>
      </div>

<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>


