<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Servicios */


//$this->params['breadcrumbs'][] = ['label' => 'Servicios', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->IDservicio, 'url' => ['view', 'IDservicio' => $model->IDservicio]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="servicios-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_2', [
        'model' => $model,
    ]) ?>

</div>
<!--Footer-->
