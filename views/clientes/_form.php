<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;


/* @var $this yii\web\View */
/* @var $model app\models\Clientes */
/* @var $form yii\widgets\ActiveForm */
?>
<body>


<div class="citas-form">
    <div class = "card col-12 colorManagersin text-white"> 
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('CREAR CLIENTE','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>
        
<div class="card-body">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre')->textInput(['maxlength' => true]) ?>
    <!--< ?= $form->field($tlf, 'IDtelefono_cliente') ?>-->
    
    
    <div class="form-group">
        <?= Html::submitButton('Ir a teléfonos', ['class' => 'sinborde colorManager zoom']) ?>
    </div>


    <?php ActiveForm::end(); ?>

</div>
        </div>
    </div>
    </body>