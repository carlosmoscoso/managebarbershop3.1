<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosCompras */

//$this->title = 'Update Productos Compras: ' . $model->IDproductoCompra;
//$this->params['breadcrumbs'][] = ['label' => 'Productos Compras', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->IDproductoCompra, 'url' => ['view', 'IDproductoCompra' => $model->IDproductoCompra]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-compras-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_2', [
        'model' => $model,
        'listaProducto' => $listaProducto,
         'listaProveedor' => $listaProveedor
    ]) ?>

</div>
