<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosCompras */
/* @var $form yii\widgets\ActiveForm */
?>
<body>

<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina("MODIFICAR COMPRA" ,'center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>

    <div class="centrar p-3">
        <?= Html::a('Volver',['productos-compras/index'],['class' => 'centrar card-body card col-2 m-2 colorManager zoom'])?>                  
    </div> 

<div class="justify-content-center row p-3">
    <div class="container centrar">
        
        <div class="col-4 card card-body centrar colorManagersin">
            <h3 class="text-center"> <?=$this->title = 'Se actualizará la compra de la siguiente fecha: </br>' . $model->fecha ?></h3>
            <br>

    <?php $form = ActiveForm::begin(); ?>

    
     <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::classname(), [
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>
    
    <?= $form->field($model, 'IDproducto_compra_producto')->widget(Select2::classname(), [
                'data' => $listaProducto,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del producto') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>

    <?= $form->field($model, 'IDproducto_compra_proveedor')->widget(Select2::classname(), [
                'data' => $listaProveedor,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del proveedor') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>

      <div class="form-group">
        <?= Html::submitButton('MODIFICAR', ['class' => 'sinborde colorManager zoom']) ?>
    </div>

    <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>

<div class="footer">
  ManageBarbershop 2022
  </div>
</body>
