<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosCompras */

//$this->title = 'Venta Productos ';
//$this->params['breadcrumbs'][] = ['label' => 'Productos Compras', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-compras-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listaProducto' => $listaProducto,
        'listaProveedor' => $listaProveedor
    ]) ?>

</div>
