<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosVentas */

//$this->title = 'Update Productos Ventas: ' . $model->IDproductoVenta;
//$this->params['breadcrumbs'][] = ['label' => 'Productos Ventas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->IDproductoVenta, 'url' => ['view', 'IDproductoVenta' => $model->IDproductoVenta]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="productos-ventas-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form_2', [
        'model' => $model,
        'listaProducto' => $listaProducto,
        'listaClientes' => $listaClientes,
    ]) ?>

</div>
