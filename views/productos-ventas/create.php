<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosVentas */

//$this->title = 'Create Productos Ventas';
//$this->params['breadcrumbs'][] = ['label' => 'Productos Ventas', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
?>
<div class="productos-ventas-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'listaProducto' => $listaProducto,
        'listaClientes' => $listaClientes,
    ]) ?>

</div>
