<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $model app\models\ProductosVentas */
/* @var $form yii\widgets\ActiveForm */
?>
<header>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" crossorigin="anonymous"></script>
</header>

<body>

<div class="citas-form">
    <div class = "card col-12 colorManagersin text-white"> 
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('VENTA PRODUCTO','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>
        
<div class="card-body">

    <?php $form = ActiveForm::begin(); ?>

 <?= $form->field($model, 'fecha')->widget(\kartik\date\DatePicker::classname(), [
        'type' => DatePicker::TYPE_COMPONENT_APPEND,
        'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
        'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
        'pluginOptions' => [
            'autoclose' => true,
            'format' => 'yyyy-mm-dd',
            'todayHighlight' => true
        ]
    ]); ?>
    
    <?= $form->field($model, 'IDproducto_venta_cliente')->widget(Select2::classname(), [
                'data' => $listaClientes,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del cliente') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>

    <?= $form->field($model, 'IDproducto_venta_producto')->widget(Select2::classname(), [
                'data' => $listaProducto,
                'options' => ['placeholder' => Yii::t('app', 'Nombre del cliente') ],
                'pluginOptions' => [
                    'allowClear' => true
                ],
                    ]); 
    ?>

  <div class="form-group">
        <?= Html::submitButton('GUARDAR', ['class' => 'sinborde colorManager zoom']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
        </div>
    </div>
    </body>
