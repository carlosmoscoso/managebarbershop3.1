<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\bootstrap4\Modal;
use kartik\date\DatePicker;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosVentasSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Productos Ventas';
//$this->params['breadcrumbs'][] = $this->title;
?>
<header>
    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" crossorigin="anonymous"></script>
</header>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('VENTA DE PRODUCTOS','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>
    

    <div class ="row justify-content-center ">  
            <div class = "card col-4 m-2 colorManager"> 
        <div class = "card-body">
    
               <h5 class="card-text">
                FECHA:<br> <?php $fechaActual = date('d-m-Y'); echo $fechaActual; ?> 
            </h5>
        </div>
    </div>
        
          <?= Html::a('Volver',['productos/index'],['class' => 'centrar card-body card colorManager card col-2 m-2  zoom sinborde'])?>       
          <?= Html::button('Venta producto', ['value'=>Url::to(['productos-ventas/create']),'class'=>'centrar card-body card col-2 m-2 colorManager  zoom','id'=>'modalButton']) ?>
                            <?php
                                    Modal::begin([
                                        'id'=>'modal',
                                        'size'=>'50',
                                    ]);
                                    echo "<div id='modalContent'></div>";
                                    Modal::end();
                            ?> 
         
         

    </div>
    <br>
    <br>
    <div class="container ">
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
    <div class="bg-white">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'attribute'=>'fecha',
                'value'=>'fecha',
                'format'=>'raw',
                'filter'=>DatePicker::widget ([
                'model'=> $searchModel,
                'type' => DatePicker::TYPE_COMPONENT_APPEND,
                'pickerIcon' => '<i class="fas fa-calendar-alt text-primary"></i>',
                'removeIcon' => '<i class="fas fa-trash text-danger"></i>',
                'attribute'=>'fecha',
                'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-mm-dd',
                'todayHighlight' => true
              ]
             ])
                
            ],
            [
                 'attribute' => 'IDproducto_venta_producto',
                 'value' => 'iDproductoVentaProducto.nombre',
             ],
            [
                 'attribute' => 'IDproducto_venta_cliente',
                 'value' => 'iDproductoVentaCliente.nombre',
            ],
            [
                 'attribute' => 'coste',
                 'value' => 'iDproductoVentaProducto.costeCliente' ,
             ],
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDproductoVenta' => $model->IDproductoVenta]);
                 }
            ],
        ],
    ]); ?>

   </div> 
         </div> 


<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>