<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

//$this->title = $model->IDtelefono;
//$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('TELÉFONOS','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>

    
<div class="row justify-content-center">

  <div class="container bg-white">

    <?= DetailView::widget([
        'model' => $model,
        //'listaCliente' => $listaCliente,
        'attributes' => [
           
            'telefono1',  
            'iDtelefonoCliente.nombre'
        ],
    ]) ?>
    
      </div>

    <div class="col-12 card card-body m-2">
        <div class="centrar p-12 m-2">
           
             <h3 class="colorBarber centrar">¿Quiere ACTUALIZAR o BORRAR o VOLVER?</h3>
        
        </div>
         <div class="centrar p-12 m-2">
            <p>
        <?= Html::a('Actualizar', ['update', 'IDtelefono' => $model->IDtelefono], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Borrar', ['delete', 'IDtelefono' => $model->IDtelefono], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => '¿Quieres borrar este Telefono?',
                'method' => 'post',
            ],
        ]) ?>
        <?= Html::a('Volver',['clientes/index'],['class' => ' btn  colorManagersin'])?>               
            
        </p>
         </div>    
         </div>
    
    </div>
      </div>

<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>
