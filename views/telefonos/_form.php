<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use app\models\Clientes;
use yii\helpers\ArrayHelper;
use kartik\select2\Select2;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */
/* @var $form yii\widgets\ActiveForm */
?>

<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('HOME','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>
<br>
<div class="container">
    
    <div class="card card-body">
        <p class="centrar">
            SE INCLUIRÁ ESTE TELÉFONO, SI NO QUIERE INCLUIR EL TELÉFONO DAR CLIC A VOLVER EN CASO CONTRARIO A GUARDAR 
        </p>
    </div>
    <br>
<div class="card card-body">

    <?php $form = ActiveForm::begin(); ?>
    
   

    <?= $form->field($model, 'telefono1')->textInput(['maxlength' => true]) ?>

    
    <br>

        <div class="col-12 card card-body m-2">

         <div class="centrar p-12 m-2">
            <p>
        <?= Html::submitButton('Guardar', ['class' => 'btn btn-success']) ?>
        
        <?= Html::a('Volver',['clientes/index'],['class' => ' btn  colorManagersin'])?>                  
    
            </p>
         </div>    
         </div>

    <?php ActiveForm::end(); ?>
    </div> 
</div>

<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>