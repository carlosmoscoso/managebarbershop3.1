<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use circulon\widgets\ColumnListView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\TelefonosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Telefonos';
?>

<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('CLIENTES','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>
<br>

<div class ="row justify-content-center ">          
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/home.png');?>
             </div>       
              <?= Html::a('Home',['site/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/us.png');?>
             </div>       
              <?= Html::a('Clientes',['telefonos/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>       
              <?= Html::a('Servicios',['servicios/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>       
              <?= Html::a('Proveedor',['proveedores/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>       
              <?= Html::a('Stock',['productos/index'],['class' => 'card-body card colorManager'])?>
        </div>
    </div>
    
</div>

<br>
<br>

<div class ="row justify-content-center">  
    <?= Html::a('Crear Cliente ', ['telefonos/create'], ['class' => 'card-body card col-2 m-2 Bcrear text-white']) ?>
    <?= Html::a('Crear Cliente 1 ', ['clientes/create'], ['class' => 'card-body card col-2 m-2 Bcrear text-white']) ?>
    <?= Html::a('Modificar cliente',['telefonos/modificar'],['class' => 'card-body card col-2 m-2 Bmodificar text-white'])?>
</div>
<br>
<br>

    <div class="row justify-content-center">
        <div class="col align-self-lg-start">
        <?=
        ColumnListView::widget([
            'pager' => [
                'class' => \yii\bootstrap4\LinkPager::class
            ],
            'dataProvider' => $dp,
            'itemView' => '_index', //tiene que llamarse como el fichero
            'layout' => "{items}\n{summary}\n{pager}",
             'columns'=> 3,
        ]);
        ?>

    </div>
</div>


