<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Telefonos */

//$this->title = 'Modificar Clientes: ' . $model->IDtelefono;
//$this->params['breadcrumbs'][] = ['label' => 'Telefonos', 'url' => ['index']];
//$this->params['breadcrumbs'][] = ['label' => $model->IDtelefono, 'url' => ['view', 'IDtelefono' => $model->IDtelefono]];
//$this->params['breadcrumbs'][] = 'Update';
?>
<div class="telefonos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        //'listaCliente' => $listaCliente,
        
    ]) ?>

</div>
