<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProveedoresSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

//$this->title = 'Clientes';
//$this->params['breadcrumbs'][] = $this->title;
?>

<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('MODIFICAR CLIENTES','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>

<body>
<br>
<div class="proveedores-index">

    <h1><?= Html::encode($this->title) ?></h1>

<div class ="row justify-content-center ">          
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/home.png');?>
             </div>       
              <?= Html::a('Home',['site/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/us.png');?>
             </div>       
              <?= Html::a('Clientes',['telefonos/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>       
              <?= Html::a('Servicios',['servicios/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>       
              <?= Html::a('Proveedor',['proveedores/index'],['class' => 'card-body card colorManager'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom sinborde"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>       
              <?= Html::a('Stock',['productos/index'],['class' => 'card-body card colorManager'])?>
        </div>
    </div>
    
</div>
    
    <br>
    <div class ="row justify-content-center ">              
            <div class = "card col-2 m-2  zoom sinborde"> 
                        <?= Html::a('Volver',['telefonos/index'],['class' => 'card-body card Bvolver'])?>       
            </div>  
            <div class = "card col-2 m-2  zoom sinborde">                
                        <?= Html::a('Crear',['telefonos/create'],['class' => 'card-body card Bcrear'])?>         
            </div>
    </div>
    <br>
    <br>
    
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'IDtelefono',
            'telefono1',
            'telefono1',
            [
                 'attribute' => 'IDtelefono_cliente',
                 'value' => 'iDtelefonoCliente.nombre',
             ],    
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action,  $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'IDtelefono' => $model->IDtelefono]);
                 }
            ],
        ],
    ]); ?>


</div>
