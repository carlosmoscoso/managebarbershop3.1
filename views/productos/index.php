<?php

use circulon\widgets\ColumnListView;
use yii\helpers\Html;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProductosSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Productos';
//$this->params['breadcrumbs'][] = $this->title;
?>
<body>
<?php
class CabeceraPagina {
  private $titulo;
  private $ubicacion;
  private $colorFuente;
  private $colorFondo;
  public function __construct($titulo,$ubica,$colorFuen,$colorFond)
  {
    $this->titulo=$titulo;
    $this->ubicacion=$ubica;
    $this->colorFuente=$colorFuen;
    $this->colorFondo=$colorFond;
  }
  public function mostrarHeader()
  {
    echo '<div style="font-size:30px;text-align:'.$this->ubicacion.';color:';
    echo $this->colorFuente.';background-color:'.$this->colorFondo.'">';
    echo $this->titulo;
    echo '</div>';

  }
}

$cabecera=new CabeceraPagina('PRODUCTOS','center','#ffffff','#11264D');
$cabecera->mostrarHeader();

?>



<div class ="row justify-content-center  p-2  ">          
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/home.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('HOME',['site/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager "> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/us.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('CLIENTES',['clientes/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/ser.png');?>
             </div>
            <hr class="colorBarber">
              <?= Html::a('SERVICIOS',['servicios/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/pro.png');?>
             </div>     
            <hr class="colorBarber">
              <?= Html::a('PROVEEDORES',['proveedores/index'],['class' => 'card-body card colorManager2'])?>
        </div>
     </div>
    
    <div class = "card col-2 m-2  zoom colorManager"> 
        <div class = "card-body">
        
             <div class = "card-body card sinborde negro">
          <?php echo Html::img('@web/image/sto.png');?>
             </div>
            <hr class="colorBarber">
            
              <?= Html::a('PRODUCTOS',['productos/index'],['class' => 'card-body card colorManager2'])?>
        </div>
    </div>
    
</div>

    <br>

<div class ="row justify-content-center  p-2">
       
        <div class = "card col-2 m-2 colorManager"> 
        <div class = "card-body">
    
               <h5 class="card-text">
                FECHA:<br> <?php $fechaActual = date('d-m-Y'); echo $fechaActual; ?> 
            </h5>
        </div>
    </div>
        <?= Html::button('Crear producto', ['value'=>Url::to(['productos/create']),'class'=>'centrar card-body card col-2 m-2 colorManager  zoom','id'=>'modalButton']) ?>
            <?php
                    Modal::begin([
                        'id'=>'modal',
                        'size'=>'50',
                    ]);
                    echo "<div id='modalContent'></div>";
                    Modal::end();
            ?>                     
       <?= Html::a('Modificar productos',['productos/modificar'],['class' => 'centrar card-body card col-2 m-2 colorManager  zoom'])?>
       <?= Html::a('Compras de productos',['productos-compras/index'],['class' => 'centrar card-body card col-2 m-2 colorManager  zoom'])?>
       <?= Html::a('Ventas de productos',['productos-ventas/index'],['class' => 'centrar card-body card col-2 m-2 colorManager  zoom'])?>

</div>

    <div class="row justify-content-center ">
        <h4 class="centrar card card-body sinborde m-2  "> Resumen de beneficio, gasto, total por mes </h4> 
    </div>
    
    <div class ="row justify-content-center ">
        <h2 class="card-body card col-3 m-2 colorManager "> 2022 </h2> 
    </div>
     
    <div class="row justify-content-center  p-3">
        
            
            <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">ENERO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosEne); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosEne); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosEne); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">FEBRERO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosFeb); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosFeb); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosFeb); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">MARZO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosMar); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosMar); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosMar); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">ABRIL</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosAbr); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosAbr); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosAbr); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">MAYO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosMay); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosMay); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosMay); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">JUNIO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosJun); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosJun); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosJun); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">JULIO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosJul); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosJul); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosJul); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">AGOSTO</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosAgo); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosAgo); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosAgo); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">SEPTIEMBRE</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosSep); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosSep); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosSep); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">OCTUBRE</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosOct); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosOct); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosOct); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">NOVIEMBRE</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosNov); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosNov); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosNov); ?> €</p>
            </div>
             </div>
            </div>
        
        <div class="centrar card-body card col-2 m-2 colorManager"> 
                
           <div class="row justify-content-center manager2 ">
                <div class="col-12">
                 <p class="card-title text-center w-100 colorManagersin h-100 ">DICIEMBRE</p>   
            </div> 
            <div class="col-12">
                 <p class="card-title text-center w-100 colorazul1 h-100 text-black-50">GASTOS: <br> <?=  implode(" ", $totalGastoProductosDic); ?> €</p>   
            </div> 
            <div class="col-12">
        <p class="card-text text-center  colorazul2 h-100 ">INGRESOS: <br> <?=  implode(" ", $totalBeneficioProductosDic); ?> €</p>
            </div> 
            <div class="col-12">
        <p class="card-title text-center w-100 colorazul3 h-100">IMPORTE NETO: <br><?=  implode(" ", $totalBeneficioNetoProductosDic); ?> €</p>
            </div>
             </div>
            </div>
        
        
        
    </div>
    <div class="row justify-content-center ">
        <h4 class="centrar card card-body sinborde m-2  "> Productos </h4> 
    </div>

    <div class="row justify-content-center">
        <div class="col-16 card-body ">
            <div class="container">
        <?=
        ColumnListView::widget([
            'pager' => [
                'class' => \yii\bootstrap4\LinkPager::class
            ],
            'dataProvider' => $dp,
            'itemView' => '_index', //tiene que llamarse como el fichero
            'layout' => "{items}\n{pager}",
             'columns'=> 2,
            
        ]);
        ?>
           </div> 
    </div> 
</div>

<br>
<!--Footer-->
<div class="footer">
  ManageBarbershop 2022
  </div>
</body>