<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductosVentas;

/**
 * ProductosVentasSearch represents the model behind the search form of `app\models\ProductosVentas`.
 */
class ProductosVentasSearch extends ProductosVentas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDproductoVenta',], 'integer'],
            [['fecha','IDproducto_venta_cliente', 'IDproducto_venta_producto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductosVentas::find()->orderBy('fecha desc');;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>['pageSize'=>8],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query ->joinWith('iDproductoVentaCliente');
        $query ->joinWith('iDproductoVentaProducto');
        
        // grid filtering conditions
        $query->andFilterWhere([
            'IDproductoVenta' => $this->IDproductoVenta,
            'fecha' => $this->fecha,
        ]);
        
         $query->andFilterWhere(['=', 'clientes.nombre',$this->IDproducto_venta_cliente]);
         $query->andFilterWhere(['=', 'productos.nombre',$this->IDproducto_venta_producto]);

        return $dataProvider;
    }
}
