<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos_compras".
 *
 * @property int $IDproductoCompra
 * @property string $fecha
 * @property int|null $IDproducto_compra_proveedor
 * @property int|null $IDproducto_compra_producto
 *
 * @property Productos $iDproductoCompraProducto
 * @property Proveedores $iDproductoCompraProveedor
 */
class ProductosCompras extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos_compras';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha'], 'required'],
            [['fecha'], 'safe'],
            [['IDproducto_compra_proveedor', 'IDproducto_compra_producto'], 'integer'],
            [['IDproducto_compra_producto'], 'exist', 'skipOnError' => true, 'targetClass' => Productos::className(), 'targetAttribute' => ['IDproducto_compra_producto' => 'IDproducto']],
            [['IDproducto_compra_proveedor'], 'exist', 'skipOnError' => true, 'targetClass' => Proveedores::className(), 'targetAttribute' => ['IDproducto_compra_proveedor' => 'IDproveedor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDproductoCompra' => 'ID producto Compra',
            'fecha' => 'Fecha',
            'IDproducto_compra_proveedor' => 'Proveedor',
            'IDproducto_compra_producto' => 'Producto',
        ];
    }

    /**
     * Gets query for [[IDproductoCompraProducto]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproductoCompraProducto()
    {
        return $this->hasOne(Productos::className(), ['IDproducto' => 'IDproducto_compra_producto']);
    }

    /**
     * Gets query for [[IDproductoCompraProveedor]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDproductoCompraProveedor()
    {
        return $this->hasOne(Proveedores::className(), ['IDproveedor' => 'IDproducto_compra_proveedor']);
    }
}
