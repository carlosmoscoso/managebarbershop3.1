<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "servicios".
 *
 * @property int $IDservicio
 * @property string $nombre
 * @property float $coste
 *
 * @property Citas[] $citas
 */
class Servicios extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'servicios';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'coste'], 'required'],
            [['coste'], 'number'],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'match','pattern'=>'/^[a-z,.\s-]+$/i'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDservicio' => 'ID servicio',
            'nombre' => 'Nombre',
            'coste' => 'Coste',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['IDcitas_servicio' => 'IDservicio']);
    }
}
