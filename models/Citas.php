<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "citas".
 *
 * @property int $IDcitas
 * @property string $fecha
 * @property string $hora
 * @property int|null $IDcitas_cliente
 * @property int|null $IDcitas_servicio
 *
 * @property Clientes $iDcitasCliente
 * @property Servicios $iDcitasServicio
 */
class Citas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'citas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora'], 'required'],
            [['fecha', 'hora'], 'safe'],
            [['fecha','hora','IDcitas_cliente','IDcitas_servicio'], 'string', 'max' => 255],
            [['IDcitas_cliente', 'IDcitas_servicio'], 'integer'],
            [['IDcitas_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDcitas_cliente' => 'IDcliente']],
            [['IDcitas_servicio'], 'exist', 'skipOnError' => true, 'targetClass' => Servicios::className(), 'targetAttribute' => ['IDcitas_servicio' => 'IDservicio']],
            
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDcitas' => 'ID citas',
            'fecha' => 'Fecha',
            'hora' => 'Hora',
            'IDcitas_cliente' => 'Nombre',
            'IDcitas_servicio' => 'Servicio',
        ];
    }

    /**
     * Gets query for [[IDcitasCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDcitasCliente()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDcitas_cliente']);
    }

    /**
     * Gets query for [[IDcitasServicio]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDcitasServicio()
    {
        return $this->hasOne(Servicios::className(), ['IDservicio' => 'IDcitas_servicio']);
    }
    
    
}
