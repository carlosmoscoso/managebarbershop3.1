<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $IDcliente
 * @property string $nombre
 *
 * @property Citas[] $citas
 * @property ProductosVentas[] $productosVentas
 * @property Telefonos[] $telefonos
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre'], 'required'],
            [['nombre'], 'string', 'max' => 50],
            [['nombre'], 'match','pattern'=>'/^[a-z,.\s-]+$/i'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDcliente' => 'ID cliente',
            'nombre' => 'Nombre',
        ];
    }

    /**
     * Gets query for [[Citas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCitas()
    {
        return $this->hasMany(Citas::className(), ['IDcitas_cliente' => 'IDcliente']);
    }

    /**
     * Gets query for [[ProductosVentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosVentas()
    {
        return $this->hasMany(ProductosVentas::className(), ['IDproducto_venta_cliente' => 'IDcliente']);
    }

    /**
     * Gets query for [[Telefonos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTelefonos()
    {
        return $this->hasMany(Telefonos::className(), ['IDtelefono_cliente' => 'IDcliente']);
    }
    
    public function getIDclienteTelefono()
    {
        return $this->hasOne(Telefonos::className(), ['IDtelefono_cliente' => 'IDcliente']);
    }
    
}
