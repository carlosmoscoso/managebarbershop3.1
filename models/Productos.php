<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "productos".
 *
 * @property int $IDproducto
 * @property string $nombre
 * @property float $costeProveedores
 * @property float $costeCliente
 * @property int $cantidad
 *
 * @property ProductosCompras[] $productosCompras
 * @property ProductosVentas[] $productosVentas
 */
class Productos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'productos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'costeProveedores', 'costeCliente', 'cantidad'], 'required'],
            [['costeProveedores', 'costeCliente','cantidad'], 'number'],
            [['nombre'], 'string', 'max' => 250],
            [['nombre'], 'match','pattern'=>'/^[a-z,.\s-]+$/i'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDproducto' => 'I Dproducto',
            'nombre' => 'Nombre',
            'costeProveedores' => 'Coste Proveedores',
            'costeCliente' => 'Coste Cliente',
            'cantidad' => 'Cantidad',
        ];
    }

    /**
     * Gets query for [[ProductosCompras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosCompras()
    {
        return $this->hasMany(ProductosCompras::className(), ['IDproducto_compra_producto' => 'IDproducto']);
    }

    /**
     * Gets query for [[ProductosVentas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosVentas()
    {
        return $this->hasMany(ProductosVentas::className(), ['IDproducto_venta_producto' => 'IDproducto']);
    }
}
