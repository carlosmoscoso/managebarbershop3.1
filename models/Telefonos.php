<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "telefonos".
 *
 * @property int $IDtelefono
 * @property string $telefono1
 * @property string $telefono2
 * @property int|null $IDtelefono_cliente
 *
 * @property Clientes $iDtelefonoCliente
 */
class Telefonos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'telefonos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['telefono1', 'IDtelefono_cliente'], 'required'],
            [['IDtelefono_cliente'], 'integer'],
            [['telefono1', 'telefono2'], 'string', 'max' => 9],
            [['telefono1', 'telefono2'],'match', 'pattern'=>'/^[0-9]{9}/'],
            [['IDtelefono_cliente'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['IDtelefono_cliente' => 'IDcliente']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDtelefono' => 'ID telefono',
            'telefono1' => 'Telefono1',
            'IDtelefono_cliente' => 'Nombre del cliente',
        ];
    }

    /**
     * Gets query for [[IDtelefonoCliente]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIDtelefonoCliente()
    {
        return $this->hasOne(Clientes::className(), ['IDcliente' => 'IDtelefono_cliente']);
    }
}
