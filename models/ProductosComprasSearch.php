<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\ProductosCompras;

/**
 * ProductosComprasSearch represents the model behind the search form of `app\models\ProductosCompras`.
 */
class ProductosComprasSearch extends ProductosCompras
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDproductoCompra'], 'integer'],
            [['fecha','IDproducto_compra_proveedor', 'IDproducto_compra_producto'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ProductosCompras::find()->orderBy('fecha desc');;

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>['pageSize'=>8],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        $query ->joinWith('iDproductoCompraProveedor');
        $query ->joinWith('iDproductoCompraProducto');
        // grid filtering conditions
        $query->andFilterWhere([
            'IDproductoCompra' => $this->IDproductoCompra,
            'fecha' => $this->fecha,
        ]);
         $query->andFilterWhere(['=', 'proveedores.nombre',$this->IDproducto_compra_proveedor]);
         $query->andFilterWhere(['=', 'productos.nombre',$this->IDproducto_compra_producto]);

        return $dataProvider;
    }
}
