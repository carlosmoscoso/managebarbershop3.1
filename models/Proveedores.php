<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "proveedores".
 *
 * @property int $IDproveedor
 * @property string $nombre
 * @property string $localizacion
 * @property string $telefono
 *
 * @property ProductosCompras[] $productosCompras
 */
class Proveedores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'proveedores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'localizacion', 'telefono'], 'required'],
            [['nombre'], 'string', 'max' => 20],
            [['nombre'], 'match','pattern'=>'/^[a-z,.\s-]+$/i'],
            [['localizacion'], 'string', 'max' => 50],
            [['telefono'], 'string', 'max' => 9],
            [['telefono'],'match', 'pattern'=>'/^[0-9]{9}/'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'IDproveedor' => 'I Dproveedor',
            'nombre' => 'Nombre',
            'localizacion' => 'Localizacion',
            'telefono' => 'Telefono',
        ];
    }

    /**
     * Gets query for [[ProductosCompras]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductosCompras()
    {
        return $this->hasMany(ProductosCompras::className(), ['IDproducto_compra_proveedor' => 'IDproveedor']);
    }
}
