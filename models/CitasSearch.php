<?php

namespace app\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Citas;
use app\models\Clientes;

/**
 * CitasSearch represents the model behind the search form of `app\models\Citas`.
 */
class CitasSearch extends Citas
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['IDcitas'], 'integer'],
            [['fecha', 'hora', 'IDcitas_cliente', 'IDcitas_servicio'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Citas::find()->orderBy('fecha desc');
        

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' =>['pageSize'=>5],
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }
        
        $query ->joinWith('iDcitasCliente');
        $query ->joinWith('iDcitasServicio');

        // grid filtering conditions
        $query->andFilterWhere([
            'IDcitas' => $this->IDcitas,
            'fecha' => $this->fecha,
            'hora' => $this->hora,
            
        ]);
        
        $query->andFilterWhere(['=', 'clientes.nombre',$this->IDcitas_cliente]);
        $query->andFilterWhere(['=', 'servicios.nombre',$this->IDcitas_servicio]);

        return $dataProvider;
    }
}
